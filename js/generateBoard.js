import { state } from './state';

function generateBoard(gameBoardElement) {
  for (let i = 0; i < state.cols; i++) {
    for (let j = 0; j < state.rows; j++) {
      generateSquare(i, j, gameBoardElement);
    }
  }
}

function generateSquare(col, row, gameBoardElement) {
  let square = document.createElement('div');
  square.classList.add("square");
  square.id = `${col}${row}`;
  gameBoardElement.appendChild(square);

  let x = row * state.squareSize;
  let y = col * state.squareSize;

  square.style.left = `${x}px`;
  square.style.top = `${y}px`;
}

export default generateBoard;