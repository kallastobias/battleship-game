import { state } from './state';

function generateShipLocation() {
  state.ships.forEach(ship => {
    let shipLocation = placeOutShip(ship.shipLen);
    while (collision(shipLocation)) {
      shipLocation = placeOutShip(ship.shipLen);
    };
    ship.location = shipLocation;
  });
} 

function placeOutShip(shipLen) {
  const direction = generateRandomNum(2);
  const col = generateRandomNum(state.cols);
  const row = generateRandomNum(state.rows, shipLen);

  const shipPos = [];
  for (let i = 0; i < shipLen; i++) {
    if (direction === 0) {
      shipPos.push(`${col}${row + i}`);
    }
    if (direction === 1) {
      shipPos.push(`${row + i}${col}`);
    }
  }
  return shipPos;
}

function collision(shipLocation) {
  for (let i = 0; i < state.ships.length; i++) {
    const ship = state.ships[i];
    for (let j = 0; j < shipLocation.length; j++) {
      if (ship.location.indexOf(shipLocation[j]) >= 0) {
        return true;
      }
    }
  }
  return false;
}

function generateRandomNum(num, shipSize = 0) {
  return Math.floor(Math.random() * (num - shipSize));
}

export default generateShipLocation;