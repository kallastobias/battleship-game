import generateBoard from './generateBoard';
import generateShipLocation from './generateShips';
import fireTorpedo from './fireTorpedo';
import { state } from './state';
import { ships } from './ships';

const gameBoardElement = document.getElementById('gameboard');
const startNewGameBtn = document.getElementById('startNewGame');
const revealAllCellsBtn = document.getElementById('revealAllCells');
const fireFeedback = document.querySelector('.fireFeedback');

startNewGameBtn.addEventListener('click', startNewGame)
revealAllCellsBtn.addEventListener('click', revealAllCells);

function startNewGame() {
  // initialize a new game
  state.reset();
  fireFeedback.textContent = 'Start shooting some torpedos!';
  gameBoardElement.addEventListener('click', fireTorpedo);
  generateBoard(gameBoardElement);
  generateShipLocation()
}

function revealAllCells() {
  const getAllShipsLocation = state.ships.reduce((acc, ship) => acc.concat(ship.location), []);
  const squares = document.querySelectorAll('.square');

  for (let i = 0; i < squares.length; i++) {
    const id = squares[i].id;
    if (getAllShipsLocation.includes(id)) {
      squares[i].classList.add('reveal')
    }
  }

  gameBoardElement.removeEventListener('click', fireTorpedo);
}

startNewGame();