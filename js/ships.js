export const ships = [
  {
    name: 'Patrol Boat',
    shipLen: 2,
    location: [0, 0],
    hit: [0, 0]
  },
  {
    name: 'Destroyer',
    shipLen: 3,
    location: [0, 0, 0],
    hit: [0, 0, 0]
  },
  {
    name: 'Submarine',
    shipLen: 3,
    location: [0, 0, 0],
    hit: [0, 0, 0],
  },
  {
    name: 'Battleship',
    shipLen: 4,
    location: [0, 0, 0, 0],
    hit: [0, 0, 0, 0],
  },
  {
    name: 'Carrier',
    shipLen: 5,
    location: [0, 0, 0, 0, 0],
    hit: [0, 0, 0, 0, 0],
  },
]