import { ships } from './ships';

export const state = {
  rows: 10,
  cols: 10,
  squareSize: 50,
  ships: JSON.parse(JSON.stringify(ships)),
  shipsSunken: 0,
  reset: function() {
    this.ships = JSON.parse(JSON.stringify(ships));
    this.shipsSunken = 0;
  }
}