import { state } from './state';
const fireFeedback = document.querySelector('.fireFeedback');

function fireTorpedo(event) {
  if (!event.target || !event.target.nodeName === "DIV") { return };

  const userClickCoordinate = event.target.id;

  for (let i = 0; i < state.ships.length; i++) {
    const ship = state.ships[i];
    if (verifyHit(event, ship, userClickCoordinate)) { return };
  }

  if (state.shipsSunken === state.ships.length
    || event.target.classList.contains('hit')) { return }

  event.target.classList.add('miss');
  fireFeedback.textContent = 'MISS';
}

function verifyHit(event, ship, userClickCoordinate) {
  if (!ship.location.includes(userClickCoordinate)
    || event.target.classList.contains('hit')) { return false };

  const idx = ship.location.indexOf(userClickCoordinate);
  ship.hit[idx] = 'hit';

  event.target.classList.add('hit');
  fireFeedback.textContent = 'HIT';

  if (hasSunk(ship)) {
    fireFeedback.textContent = `You just sunk my ${ship.name}`;
    state.shipsSunken++;
  }

  if (state.shipsSunken === state.ships.length) {
    fireFeedback.textContent = 'You just sunk all my ships. Well done. Play again?';
  }
  
  return true;
}

function hasSunk(ship) {
  return ship.hit.every(h => h === 'hit');
}


export default fireTorpedo;