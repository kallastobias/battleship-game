# Battleship

Your task is to write the classic battleship game in JavaScript.

Learn about the game and what the rules are here: https://en.wikipedia.org/wiki/Battleship_(game)

Use the provided html and js files as a starting point, or just create your own from scratch: your choice.

---

Requirements:

- Draw a grid that is the game board
- When clicking cells, they should indicate whether it's a hit or a miss
- When all ships are sunk, the game is over
- Write code that will dynamically put the ships on the game board when initializing a new game

---

## git clone and start local server
```bash
$ git clone https://kallastobias@bitbucket.org/kallastobias/battleship-game.git
$ cd battleship-game
$ npm install
$ npm run start
```
And now you are ready to play :)